use std::{f32::consts::{PI, TAU}, sync::{Arc, RwLock}};

use nako::{glam::{Vec2, Vec3}, operations::{planar::{modifiers2d::Translate2d, primitives2d::Box2d}, volumetric::{Color, Round, Union}}, stream::{PrimaryStream2d, SecondaryStream2d}};
use nakorender::{backend::{Backend, LayerId2d, LayerInfo}, camera::Camera2d, marp::MarpBackend, winit::{event::{Event, MouseButton, WindowEvent}, event_loop::EventLoop, window::Window}};

use crate::Config;

struct VoiceBar{
    layer_id: LayerId2d,
    layerinfo: LayerInfo,
    has_changed: bool,
}

struct Background{
    layer_id: LayerId2d,
    layerinfo: LayerInfo,
}

impl Background{
    fn new_for_ext(&self, extent: (u32, u32)) -> PrimaryStream2d{
	PrimaryStream2d::new()
	    .push(
		SecondaryStream2d::new(
		    Union,
		    Box2d{extent: Vec2::new(1.0, 1.0)}
		).push_mod(Color(Vec3::new(0.035, 0.02, 0.03)))
		    .push_mod(Translate2d(Vec2::new(0.5, 0.5)))
		    .build()
	    ).build()
    }
}

pub struct UI{
    config: Arc<RwLock<Config>>,
    renderer: MarpBackend,
    pub window: Window,

    bar: VoiceBar,
    bg: Background,
    
    is_editing: bool,
}

impl UI{
    pub fn new(window: Window, eventloop: &EventLoop<()>, config: Arc<RwLock<Config>>) -> Self{
	let mut renderer = MarpBackend::new(&window, eventloop);

	let bar = VoiceBar{
	    layer_id: renderer.new_layer_2d(),
	    layerinfo: LayerInfo{
		extent: (2,2),
		location: (0,0)
	    },
	    has_changed: true,
	};

	let bg = Background{
	    layer_id: renderer.new_layer_2d(),
	    layerinfo: LayerInfo{
		extent: (1,1),
		location: (0,0)
	    }
	};
	
	renderer.set_layer_order(&[bg.layer_id.into(), bar.layer_id.into()]);
	
	UI{
	    config,
	    renderer,
	    window,

	    bar,
	    bg,
	    
	    is_editing: false,
	}
    }
    
    pub fn on_event(&mut self, event: &Event<()>){
	match event{
	    Event::WindowEvent{window_id: _, event: WindowEvent::MouseInput{button: MouseButton::Left, device_id: _, modifiers: _, state}} => {
		match state{
		    nakorender::winit::event::ElementState::Pressed => self.is_editing = true,
		    nakorender::winit::event::ElementState::Released => self.is_editing = false,
		}
	    }
	    Event::WindowEvent{window_id: _, event: WindowEvent::Resized(new_size)} => {
		println!("Resize to : {:?}", new_size);

		//Resize Bar
		self.bar.layerinfo = LayerInfo{
		    extent: (25, 25),
		    location: (new_size.width as usize / 2, new_size.height as usize / 2) 
		};
		
		self.renderer.set_layer_info(self.bar.layer_id.into(), self.bar.layerinfo);
		self.renderer.update_camera_2d(self.bar.layer_id, Camera2d{
		    extent: Vec2::new(2.0, 2.0),
		    location: Vec2::new(0.0, 0.0),
		    rotation: 0.0
		});
		self.bar.has_changed = true;

		//update background
		self.bg.layerinfo = LayerInfo{
		    extent: (new_size.width as usize, new_size.height as usize),
		    location: (0,0)
		};
		self.renderer.set_layer_info(self.bg.layer_id.into(), self.bg.layerinfo);
		self.renderer.update_sdf_2d(self.bg.layer_id, self.bg.new_for_ext((new_size.width, new_size.height)));
		self.renderer.update_camera_2d(self.bg.layer_id, Camera2d{
		    extent: Vec2::ONE,
		    location: Vec2::ZERO,
		    rotation: 0.0
		});
		
		self.renderer.resize(&self.window);
	    },
	    Event::WindowEvent{window_id: _, event: WindowEvent::CursorMoved{device_id: _, modifiers: _, position}} => {
		
		//If currently editing, change the frequencies and modulation accordingly
		if self.is_editing{	    
		    let change = Vec2::new(
			(position.x as f32 / self.window.inner_size().width as f32).clamp(0.0, 1.0),
			(position.y as f32 / self.window.inner_size().height as f32).clamp(0.0, 1.0),
		    );

		    let mut conf = self.config.write().unwrap();

		    for i in 0..16{
			conf.voices[i].primary_freq = conf.base_freq;
			conf.voices[i].secondary_freq = (
			    200.0 * change.x + //Mai secondary modulation
				(i as f32/ 16.0) * 1000.0 * change.y.powf(4.0) //per voice detune
			).clamp(0.0, 1000.0); 
		    }
		    
		    println!("new config: ");
		    for v in conf.voices.iter(){
			println!("    {}Hz/{}Hz @ {}", v.primary_freq, v.secondary_freq, v.loudness);
		    }

		    drop(conf);


		    //Update bar position
		    self.bar.layerinfo = LayerInfo{
			extent: (50, 50),
			location: (
			    (position.x as isize - 25).clamp(0, isize::MAX) as usize,
			    (position.y as isize - 25).clamp(0, isize::MAX) as usize
			) 
		    };
		    self.renderer.set_layer_info(self.bar.layer_id.into(), self.bar.layerinfo);
		    self.renderer.update_sdf_2d(
			self.bar.layer_id,
			PrimaryStream2d::new()
			    .push(
				SecondaryStream2d::new(
				    Union,
				    Box2d{extent: Vec2::new(change.x, change.x)}
				).push_mod(Round{radius: 1.0 - change.x})
				    .push_mod(Color(Vec3::new(1.0, 1.0, 0.0) * (1.0 - change.y)))
				    .push_mod(Translate2d(Vec2::new(1.0, 1.0)))
				    .build()
			    ).build()
		    );
		    self.renderer.update_camera_2d(self.bar.layer_id, Camera2d{
			extent: Vec2::new(2.0, 2.0),
			location: Vec2::ZERO,
			rotation: 0.0
		    });
		}
	    }
	    _ => {}
	}
    }
    
    pub fn redraw(&mut self){
	//Catch everything that has changed, update those sdfs
	if self.bar.has_changed{
	    println!("Update sdf");
	    let new_sdf = PrimaryStream2d::new()
		.push(
		    SecondaryStream2d::new(
			Union,
			Box2d{extent: Vec2::new(1.0, 1.0)}
		    ).push_mod(Color(Vec3::new(1.0, 0.8, 0.6)))
			.push_mod(Translate2d(Vec2::new(1.0, 1.0)))
			.build()
		)
		.build();
	    self.renderer.update_sdf_2d(self.bar.layer_id, new_sdf);
	    self.bar.has_changed = false;
	}
	self.renderer.render(&self.window);
    }
}
