use std::{f32::consts::{PI, TAU}, sync::{Arc, RwLock}};

use hostcpal::Playable;
use nakorender::winit::{event::{DeviceEvent, ElementState, Event, KeyboardInput, VirtualKeyCode, WindowEvent}, event_loop::ControlFlow};
use ui::UI;

mod ui;

#[derive(Clone, Copy)]
struct Voice{
    primary_freq: f32,
    secondary_freq: f32,
    loudness: f32,
}

pub struct Config{
    base_freq: f32,
    next_lower: f32,
    next_higher: f32,

    wave_freq: f32,
    
    ///Frequency and loudness configuration
    voices: [Voice; 16]
}

impl Config{
    pub fn new() -> Arc<RwLock<Config>>{
	Arc::new(RwLock::new(Config{
	    base_freq: 440.0,
	    next_higher:  659.25,
	    next_lower:  293.66,
	    wave_freq: 50.0,
	    voices: [Voice{primary_freq: 440.0, secondary_freq: 5.0, loudness: 1.0 / 16.0}; 16]
	}))
    }

    ///n is how many half tones we are away from the 440Hz A.
    ///Taken from here: https://pages.mtu.edu/~suits/NoteFreqCalcs.html
    pub fn set_base_freq(&mut self, n: isize){
	let n = n as f32;
	let a: f32 = 2.0f32.powf(1.0 / 12.0);

	let f_base = 440.0 * a.powf(n);
	let f_hight = 440.0 * a.powf(n + 2.0);
	let f_low = 440.0 * a.powf(n - 2.0);

	self.base_freq = f_base;
	self.next_higher = f_hight;
	self.next_lower = f_low;
    }
}

struct SinStack{
    config: Arc<RwLock<Config>>,
    wave_phase: f32,
    phases: [(f32, f32); 16]
}

impl SinStack{
    fn new(config: Arc<RwLock<Config>>) -> Self{
	SinStack{
	    config,
	    wave_phase: 0.0,
	    phases: [(0.0, 0.0); 16]
	}
    }
}

impl Playable for SinStack{
    fn process(&mut self, data: &mut [f32], channels: usize, sample_rate: usize) {
	let sample_time = 1.0 / sample_rate as f32;

	let wave_freq = self.config.read().unwrap().wave_freq;
	
	for channel in data.chunks_mut(channels){

	    //Copy current config for fast reads
	    let voices: [Voice; 16] = self.config.read().unwrap().voices;
	    let sample = self.phases.iter().fold(self.phases[0].0, |sp, p| sp + p.1.sin()).sin();

	    for i in 0..16{
		self.phases[i].0 += voices[i].primary_freq * sample_time * TAU;
		self.phases[i].1 += voices[i].secondary_freq * sample_time * TAU;

		self.phases[i].0 = self.phases[i].0 % TAU;
		self.phases[i].1 = self.phases[i].1 % TAU;	
	    }
	    /*
	    for i in 0..16{

		//Using a moving sinus to calculate the loudness of this sample
		let loudness = (i as f32 / 16.0) * TAU + self.wave_phase;
		let loudness = (loudness.sin() + 1.0) / 2.0 / PI;
		
		sample += (self.phases[i].0 * self.phases[i].1).sin() * loudness;
		self.phases[i].0 += voices[i].primary_freq * sample_time * TAU;
		self.phases[i].1 += voices[i].secondary_freq * sample_time * TAU;

		self.phases[i].0 = self.phases[i].0 % TAU;
		self.phases[i].1 = self.phases[i].1 % TAU;
	    }
	    */
	    self.wave_phase += wave_freq * sample_time * TAU;
	    self.wave_phase = self.wave_phase % TAU;
	    
	    for s in channel{
		*s = sample;
	    }
	}
    }
}

fn main() {

    //Setup audio playback
    let host = hostcpal::Host::new_default();
    let mut device = host.new_device_with_properties(44100, 256, 2).unwrap();

    let config = Config::new();
    
    device.set_player(SinStack::new(config.clone())).unwrap();
    device.play().unwrap();

    let eventloop = nakorender::winit::event_loop::EventLoop::new();
    let window = nakorender::winit::window::Window::new(&eventloop).unwrap();
    window.set_cursor_visible(false);

    let mut ui = UI::new(window, &eventloop, config);
    
    eventloop.run(move |event: Event<()>, _, ctrflow|{

	*ctrflow = ControlFlow::Poll;
	ui.on_event(&event);
	
	match event{
	    Event::WindowEvent{
		window_id: _,
		event: WindowEvent::KeyboardInput{
		    device_id: _,
		    input: KeyboardInput{
			modifiers: _,
			scancode: _,
			state: ElementState::Released,
			virtual_keycode: Some(VirtualKeyCode::Escape)
		    },
		    is_synthetic: _
		}
	    } => {
		*ctrflow = ControlFlow::Exit;
	    },
            Event::MainEventsCleared => {
                ui.window.request_redraw();
            }
	    Event::RedrawRequested(_window_id) => {
		ui.redraw();
	    },
	    _ => {}
	}
    })
}
