# Sinstack
Small synthesizer written for fun. The synth basically takes 16 sinus voices that Frequency modulated a base frequency. Detune and speed of those modulation voices can be change on x/y axis. 


## Platform
Note: This is only tested on Archlinux with AMD/Intel graphics. It should compile on windows, but I'm not sure if it works.

## Running
Just execute ´cargo run --release´. A window with a square cursor should appear. Use left-click + cursor_move to change the sound.
